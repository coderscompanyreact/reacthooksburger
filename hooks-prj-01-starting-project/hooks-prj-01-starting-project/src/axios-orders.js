import axios from 'axios';

const instance = axios.create({
    baseURL: 'https://react-hooks-3df21.firebaseio.com/'
});

export default instance;